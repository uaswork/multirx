/* SpektrumProtocol Library for sending and reading DSMX 2048 protocol
 * 
 * Copyright (c) 2019, Frederik Mazur Andersen <fm@mmmi.sdu.dk>/<fm@fmazur.dk>
 *                     
 * SDU UAS Center, http://sdu.dk/uas, University of Southern Denmark 
 * All rights reserved.
 * 
 ********************************************************************************
 *  Revision
 *  2020-09-25 FMA First Version: supports statics function for packing/unpacking, but also direct transmission and reading if given serial ports and update calls
 *  2020-10-05 FMA First Version: Added interframe timing check to make receiving frames more reliable
 */

#ifndef spektrum_protocol
#define spektrum_protocol

#include <array>
#include <Arduino.h>
#include <HardwareSerial.h>

// interface definition for RC receivers. Made to make it easier to swap with other RC receivers
#define PREAMBLE 0xB2 // Preamble for SPM4648/DX9 combo.

// Specifics for Spektrum DSMX 2048
#define RC_CHANNELS 8

#define MSB_CHANNEL_SHIFT 3
#define MSB_CHANNEL_MASK 0xF
#define MSB_VALUE_SHIFT 8       // when making highbyte, we need to shift value 8 times to right, to get the MSB
#define MSB_VALUE_MASK 0x7FF    // mask for MSB for high byte value
#define LSB_VALUE_MASK 0xFF     // mask that only keeps the lower 8 bits, which is our LSB

#define FRAME_SIZE 16       /**< DSM frame size in bytes */
#define FRAME_CHANNELS 7    /**< Max supported DSM channels per frame */
// #define MAX_CHANNEL_COUNT 18  /**< Max channel count of any DSM RC */

// Defines for channels
#define CHANNEL_THROTTLE 0
#define CHANNEL_ROLL 1
#define CHANNEL_PITCH 2
#define CHANNEL_YAW 3
#define CHANNEL_KS 4
#define CHANNEL_FLIGHT_MODE 5
#define CHANNEL_AUX2 6
// What channel to indicate which, if any, pilot is connected
#define CHANNEL_AUX3_PILOT_CONNECT 7 //AUX 3
#define CHANNEL_AUX4 8
#define CHANNEL_AUX5 9
#define CHANNEL_AUX6 10
#define CHANNEL_AUX7 11

struct rcChan
{
    uint8_t _id;
    uint16_t _value; //in PPM 1000 to 2000
    rcChan() {};
    rcChan(uint8_t id, uint16_t value) : _id(id), _value(value) {};
};

struct frame
{
    uint8_t fades;
    uint8_t system;
    rcChan channels[7];
    frame() {};
};

class SpektrumProtocol {
    public:
        // A single dataframe can only support 7 channels, if we want more, we need to send extra frames with the rest
        static std::array<uint8_t, FRAME_SIZE> makeDataFrame(rcChan c1, rcChan c2, rcChan c3, rcChan c4, rcChan c5, rcChan c6, rcChan c7);
        static frame makeFrame(rcChan c1, rcChan c2, rcChan c3, rcChan c4, rcChan c5, rcChan c6, rcChan c7);
        static std::array<uint8_t, FRAME_SIZE> makeDataFrame(frame* nframe);
        static frame parseDataframe(std::array<uint8_t, FRAME_SIZE> raw);

        // framing and parsing functions and values
        void receiveData(uint8_t raw, unsigned long now);
        bool gotNewFrame();
        frame getNewFrame();
        void sendFrame(frame* nframe, HardwareSerial* output);
    private:
        // static functions
        static uint8_t makeHighByte(rcChan c);
        static uint8_t makeLowByte(rcChan c);
        static uint16_t convertTo2048Format(uint16_t ppm); // 1000 <-> 2000 values
        static uint16_t convertToPPMFormat(uint16_t value); // 1000 <-> 2000 values
        static rcChan decode_channel(uint8_t high, uint8_t low);

        // framing and parsing functions and values
        bool _gotNewFrame = false;
        frame currFrame;

        // receiving timestamps to ensure we don't mix frames
        unsigned long lastReceived = 0;
        
        enum ReceiveState { newFrame, waitPreamble };
        ReceiveState recState = waitPreamble;

        int byte_count = 0;
        std::array<uint8_t, FRAME_SIZE> receivedBuffer;                        
        std::array<uint8_t, FRAME_SIZE> transmitBuffer;                        
        unsigned char prev_byte;

};

#endif //spektrum_protocol