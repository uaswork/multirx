/* SpektrumProtocol Library for sending and reading DSMX 2048 protocol
 * 
 * Copyright (c) 2019, Frederik Mazur Andersen <fm@mmmi.sdu.dk>/<fm@fmazur.dk>
 *                     
 * SDU UAS Center, http://sdu.dk/uas, University of Southern Denmark 
 * All rights reserved.
 * 
 ********************************************************************************
 *  Revision
 *  2020-09-25 FMA First Version: supports statics function for packing/unpacking, but also direct transmission and reading if given serial ports and update calls
 *  2020-10-05 FMA Second Version: Added interframe timing check to make receiving frames more reliable
 *  2020-10-05 FMA Third Version: Fixed critical bug where lowbyte wasn't correctly converted from PPM to 2048 
 */

#include "protocol.h"

std::array<uint8_t, FRAME_SIZE> SpektrumProtocol::makeDataFrame(rcChan c1, rcChan c2, rcChan c3, rcChan c4, rcChan c5, rcChan c6, rcChan c7)
{
    // build array
    std::array<uint8_t, FRAME_SIZE> arr;

    //fade count
    arr[0] = 0;

    // preamble
    arr[1] = PREAMBLE;

    // run through each channel value and write high/low byte
    // channel 1
    arr[2] = makeHighByte(c1);
    arr[3] = makeLowByte(c1);
    // channel 2
    arr[4] = makeHighByte(c2);
    arr[5] = makeLowByte(c2);
    // channel 3
    arr[6] = makeHighByte(c3);
    arr[7] = makeLowByte(c3);
    // channel 4
    arr[8] = makeHighByte(c4);
    arr[9] = makeLowByte(c4);
    // channel 5
    arr[10] = makeHighByte(c5);
    arr[11] = makeLowByte(c5);
    // channel 6
    arr[12] = makeHighByte(c6);
    arr[13] = makeLowByte(c6);
    // channel 7
    arr[14] = makeHighByte(c7);
    arr[15] = makeLowByte(c7);

    return arr;
}

frame SpektrumProtocol::makeFrame(rcChan c1, rcChan c2, rcChan c3, rcChan c4, rcChan c5, rcChan c6, rcChan c7)
{
    // build frame
    frame nframe;

    nframe.fades = 0;
    nframe.system = PREAMBLE;

    nframe.channels[0] = c1;
    nframe.channels[1] = c2;
    nframe.channels[2] = c3;
    nframe.channels[3] = c4;
    nframe.channels[4] = c5;
    nframe.channels[5] = c6;
    nframe.channels[6] = c7;

    return nframe;
}

std::array<uint8_t, FRAME_SIZE> SpektrumProtocol::makeDataFrame(frame* nframe)
{
    // build array
    std::array<uint8_t, FRAME_SIZE> arr;

    //fade count
    arr[0] = nframe->fades;

    // preamble
    arr[1] = nframe->system;

    // fill channel data
    for (int i = 0; i < FRAME_CHANNELS; i++) {
        // fill up channels
        arr[2*i+2] = makeHighByte(nframe->channels[i]);
        arr[2*i+2+1] = makeLowByte(nframe->channels[i]);
    }
    return arr;
}

uint8_t SpektrumProtocol::makeHighByte(rcChan c)
{
    // and'ing channel id, and value respectively to ensure only the right bits gets moved and added
    return ((c._id & MSB_CHANNEL_MASK) << MSB_CHANNEL_SHIFT) | ((convertTo2048Format(c._value) & MSB_VALUE_MASK) >> MSB_VALUE_SHIFT);
}

uint8_t SpektrumProtocol::makeLowByte(rcChan c)
{
    return convertTo2048Format(c._value) & LSB_VALUE_MASK;
}

frame SpektrumProtocol::parseDataframe(std::array<uint8_t, FRAME_SIZE> raw)
{
    //expecting to have full array of 16 bytes for parsing
    frame nFrame;
    nFrame.fades = raw[0];
    nFrame.system = raw[1];

    // parse channels
    for (int i = 0; i < FRAME_CHANNELS; i++) {
        nFrame.channels[i] = decode_channel(raw[2*i+2], raw[2*i+2+1]);
    }

    return nFrame;
}

rcChan SpektrumProtocol::decode_channel(uint8_t high, uint8_t low)
{
    // decode channel
    rcChan nChan(0,0);

    // get channel id from MSB
    nChan._id = (high >> MSB_CHANNEL_SHIFT) & MSB_CHANNEL_MASK;
    // get value from MSB and LSB, channels has to be less than 12. Not supporting XPlus channel
    if (nChan._id <= RC_CHANNELS) {
        nChan._value = convertToPPMFormat(((high & 0x07) << 8) | low);
        // nChan._value = (((high & 0x07) << 8) | low);
    }
    return nChan;    
}

uint16_t SpektrumProtocol::convertTo2048Format(uint16_t ppm) 
{
    // Scaling
    //  See Specification for Spektrum Remote Receiver Interfacing Rev G 2019 January 22
    //  2048 Mode Scaling: PWM_OUT = (ServoPosition x 0.583μs) + Offset (903μs)
    //  scaled integer for decent accuracy while staying efficient (0.583us ~= 1194/2048)
    // ppm = (value * 1194) / 2048 + 903;
    // solved equation to isolate value
    return (1024 * ppm) / 597 - 308224 / 199;
}

uint16_t SpektrumProtocol::convertToPPMFormat(uint16_t value) 
{
    // Scaling
    //  See Specification for Spektrum Remote Receiver Interfacing Rev G 2019 January 22
    //  2048 Mode Scaling: PWM_OUT = (ServoPosition x 0.583μs) + Offset (903μs)
    //  scaled integer for decent accuracy while staying efficient (0.583us ~= 1194/2048)
    return (value * 1194) / 2048 + 903;
}

void SpektrumProtocol::receiveData(uint8_t raw, unsigned long now)
{
    // no matter the timing, if we have more than 5ms since last byte received, it has to be a new frame
    if ((now - lastReceived) > 5)
    {
        // reset state, and throw away current values
        byte_count = 0;
        recState = waitPreamble;
    }

    switch(recState) {
        case waitPreamble:
            if (raw == PREAMBLE) {
                receivedBuffer[0] = prev_byte; //fades
                receivedBuffer[1] = raw; //system
                byte_count = 2; // if we are at preamble, then we have missed fades, so we are 2 bytes in, of the 16 bytes

                // set state to newFrame
                recState = newFrame;
            } else {
                // if we are not at preamble, set prev byte as current, so we save fades over to when we hit preamble
                prev_byte = raw;
            }
            break;
        case newFrame:
            receivedBuffer[byte_count] = raw;
            byte_count++;

            // if byte_count is 16 now, then we have a full frame, as index 15 is last index in buffer
            if (byte_count >= FRAME_SIZE) {
                // parse frame
                currFrame = parseDataframe(receivedBuffer);
                _gotNewFrame = true;

                // reset for next frame
                byte_count = 0;
                recState = waitPreamble;
            }
            break;
    }

    //always update last received
    lastReceived = now;
}

void SpektrumProtocol::sendFrame(frame* nframe, HardwareSerial* output)
{
      // convert frame to array
  transmitBuffer = makeDataFrame(nframe);

  for (int i = 0; i < FRAME_SIZE; i++) {
    // send frames
    output->write(transmitBuffer[i]);
  }
  return;
}

bool SpektrumProtocol::gotNewFrame() 
{
    return _gotNewFrame;
}

frame SpektrumProtocol::getNewFrame()
{
    // set bool to false, as the newest frame has now been collected
    _gotNewFrame = false;
    return currFrame;
}
