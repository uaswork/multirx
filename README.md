# MultiRX
This repository contains Arduino code for merging multiple satellites into one flight controller, with the capability to modify specific channels. If none of the satellites receive data, predetermined dataframes are spoofed into the flight controller. Currently only supports Spectrum receivers. 

## OBS - Error on PCB V1
Due to error in the pins designated as TX/RX for serial1, the silkscreen is wrong. PA24 is TX_1, while PA25 is RX. This is only on the first two boards made, it has been fixed on subsequent board. 

## Install / Flash
Use platformio extension with visual studio. Then You can open it as a platformio project and at the button use the buttons to compile and flash the board with an Atmel ICE programmer. The platformio.ini file is configured with the current version of MultiRX board. See "Custom Variant" for info about how the variant was made.

## Configuration
The ``config.h`` file controls how many receivers are connected and the correct reference for pin assignments. Modify this for PCB variant used. The ``MultiRX.h`` contains the hardcoded RC message sent when there is no active receivers. Modify it for specific application use. This is also where the pilot-connected channel can be changed. ``protocol.h`` can be swapped with an header files that contains the same functions, but internally handles a different protocol. Thus easier supporting different radio protocols. 

### V2 USB option
V2 has USB and is ment to work with the bootloader to flash through the USB. Use ATMEL-ICE once to flash the program that flashes with the bootloader, and then you can update through usb. Otherwise try and use arduino IDE to flash the arduinozero bootloader over. (Has not been tested yet)

## Custom Variant
The platformio.ini file is configured to use the custom variant defined in this repo. This is to correctly map and configure the PINS for the chip with our breakouts on our custom board. In particular the serial ports. 

The variant.h and .cpp files does this configuration. The .cpp file sets/describes all the pins you need, with the attributes, chip pin designation and functionality. The .h file uses this description to make defines that can be used by the "user" in the arduino programming. As an example when the .h file defines LED_BUILTIN to (19u) it means that item number 19 (unsigned int) in the array defined in .cpp file, is the LED. So for this to match then index 19, of the array in .cpp file, should contain the correct item describing PA15 which is our LED and as such a digital output.

At the end of .cpp and .h the serial ports are defined based on the pads and the serial objects are created for easy usage in arduino. It is also possible to define COM, I2C, SPI etc. protocols if they are bought out and used by the board. However it requires the pins on the chip to support it. 

The attributes and information of each pin for the chip is found in the chips datasheet under "muxing". Has all the informations needed. 

The analog pins are defined even though they are not bought out on our board, this is because the arduino framework needed them defined to be able to build/compile. 

source: https://hackaday.io/project/8007-hack/log/41354-docs-how-the-variant-system-works-on-arduino-zero-boards   
datasheet: http://ww1.microchip.com/downloads/en/DeviceDoc/SAM_D21_DA1_Family_DataSheet_DS40001882F.pdf  

## Custom Board
The ``boards`` folder hold the board definitions for our custom boards so we can easily use them and change between boards in the ``platform.ini`` file. Do note that the ``variants_dir`` property is set in the definition so we know where to find the custom variants. 

### hwids
The hwids is set to recognize the board by vendor and product id. So platformio can find the upload port itself. A way to find the values for your board is using ``pio device list`` to see the PID/VID of your board. Needs to be done, can hold multiple entries. ATMEL-ICE should always be one of the entries. These values should also be visible in ``dmesg``.

## Code Design - High Level
``zmain.cpp`` is the main file. Here the ``MultiRX`` class is created and setup. In the loop the ``MultiRX`` class gets its update handler called that handles main program/logic. An LED is also made blinking, to help diagnose if the software faulted or is still running. The ``MultiRX`` class uses the ``protocol.cpp/h`` files and their ``SpektrumProtocol`` lib, so interface with the protocol. The main ``MultiRX`` logic only deals in the internal protocol struct ``frame``. The ``SpektrumProtocol`` lib deals with transforming that to and from the actual bytearray. This helps decouple the specific RC protocol from the main logic of the ``MultiRX`` class and in turn makes it easier to swap out RC equipment to another brand. It also means we know have a c++ library that can be used to interact with Spektrum RC protocol. 

## Equipment needed
* 1x MultiRX board
* 2x Spektrum SPM 4648 satellites.

## Flowchart
Below is a high-level flowchart of the code flow.

![multirx_mavlora_flowchart-multirx](/uploads/d245381551a259a3077360477dccab4b/multirx_mavlora_flowchart-multirx.png)

