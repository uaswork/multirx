/*
 * Copyright (c) 2020, Frederik Mazur Andersen <fm@mmmi.sdu.dk>/<fm@fmazur.dk>
 *                     Kristian Husum Terkildsen <khte@mmmi.sdu.dk>/<kristian.h.terkildsen@gmail.com>
 * SDU UAS Center, http://sdu.dk/uas, University of Southern Denmark 
 * All rights reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

#ifndef MULTIRX_FILE_H
#define MULTIRX_FILE_H

#include <Arduino.h>
#undef max // fixes for ArduinoCore bug with max and min functions
#undef min

#include <HardwareSerial.h>
#include "protocol.h"
#include "config.h"

#define RX_TIMEOUT 800 // 0.8 seconds. PX4 starts to trigger loss of RC if we go longer than 1s without packages
#define RX_LISTEN 2000 // 2 seconds.
#define DATA_DELAY 11 // 11ms == 1 frames.

// array of split ppm values for the "what receiver is connected channel". Default channel is AUX3, but another can be set in protocol.h
// each receiver will get assigned an interval between 1000 -> 2000 that channel will show if they are connected. 
#define PPM_FOR_EACH_RECEIVER ((int) (1000/RECEIVER_COUNT))

class MultiRX {

    public:
        void updateLoop();
        void setupIO();
        
    private:
        // functionalities
        void passthrough();
        void check_connection();
        void send_spoofed();
        void scan_for_connections();
        
        void sendFrameToFC(frame* nframe);
        void setPilotConnected(frame* nframe);

        // helper functions for array handling
        void turnOffAllReceivers();
        void NextReceiver();

        // timing vars
        unsigned long _last_heard;
        unsigned long _milli_curr;
        unsigned long _milli_diff;
        unsigned long _milli_prev;
        unsigned long _datamix_timeout = 0; //set to zero so we send straight away on bootup

        // index of the current active receiver and the one we are scanning. -1 == no active
        int _activeReceiver = -1;
        int _scanReceiver = 0;
        int _receiverCount = 0;

        // ppm value of connected receiver
        int _ppmWhoValues[RECEIVER_COUNT];

        // Framing lib
        SpektrumProtocol protocolLib;

        // Serials
        HardwareSerial *_input;
        HardwareSerial *_output;

        // create data to send when spoofing. 1500PPM flight_mode and pilot should be AUTO - LOITER and "No Pilot - AUTO"
        // we need two frames for DSM protocol to cover all channels, otherwise PX4 doesn't accept it
        // only send as many channels as the radio that was calibrated has. Otherwise It only accepts spoof messages after at least one passthroguh has happened from an TX.
        frame _spoofFrame1 = SpektrumProtocol::makeFrame(
                                    rcChan(CHANNEL_THROTTLE, 1500),
                                    rcChan(CHANNEL_ROLL, 1500),
                                    rcChan(CHANNEL_PITCH, 1500),
                                    rcChan(CHANNEL_YAW, 1500),
                                    rcChan(CHANNEL_FLIGHT_MODE, 1500),
                                    rcChan(CHANNEL_KS, 100),
                                    rcChan(CHANNEL_AUX3_PILOT_CONNECT, 1500));

        frame _spoofFrame2 = SpektrumProtocol::makeFrame(
                                    rcChan(CHANNEL_AUX2, 1500),
                                    rcChan(CHANNEL_AUX4, 1500),
                                    rcChan(CHANNEL_AUX5, 1500),
                                    rcChan(CHANNEL_THROTTLE, 1500),
                                    rcChan(CHANNEL_ROLL, 1500),
                                    rcChan(CHANNEL_YAW, 1500),
                                    rcChan(CHANNEL_PITCH, 1500));
};

#endif // MULTIRX_FILE_H