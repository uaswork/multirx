/*
 * Copyright (c) 2020, Frederik Mazur Andersen <fm@mmmi.sdu.dk>/<fm@fmazur.dk>
 *                     Kristian Husum Terkildsen <khte@mmmi.sdu.dk>/<kristian.h.terkildsen@gmail.com>
 * SDU UAS Center, http://sdu.dk/uas, University of Southern Denmark 
 * All rights reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

#include <Arduino.h>
#include "multirx.h"

#define BLINK_INTERVAL 200 // blink every 200ms

MultiRX multirx;
unsigned long last_blink;
int led_state;

/*
******* REMEMBER TO EDIT CONFIG.H FOR YOUR CONFIGURATION *******
*/
void setup ()
  {
    multirx.setupIO();

    // setup LED Pin
    pinMode(LED_BUILTIN, OUTPUT);
    last_blink = millis();
    led_state = HIGH;
    digitalWrite(LED_BUILTIN, led_state);
  }  // end of setup

void loop ()
  {
    // get time
    unsigned long curr_millis = millis();

    // blink led to show we are still running
    if ((curr_millis - last_blink) > BLINK_INTERVAL) {
      // flip led state
      led_state = led_state ? LOW : HIGH;
      digitalWrite(LED_BUILTIN, led_state);
      last_blink = curr_millis;
    }

    // update loop
    multirx.updateLoop();

  }  // end of loop
