/*
 * Copyright (c) 2020, Frederik Mazur Andersen <fm@mmmi.sdu.dk>/<fm@fmazur.dk>
 *                     Kristian Husum Terkildsen <khte@mmmi.sdu.dk>/<kristian.h.terkildsen@gmail.com>
 * SDU UAS Center, http://sdu.dk/uas, University of Southern Denmark 
 * All rights reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

#include "multirx.h"

void MultiRX::setupIO()
{
  // save vars
  _activeReceiver = -1; //none active from start
  _receiverCount = RECEIVER_COUNT;

  // set power mode for power pins, all start low
  for (int i = 0; i < _receiverCount; i++)
  {
    pinMode(powerPins[i], OUTPUT);
    digitalWrite(powerPins[i], LOW);
  }

  // Activate channel modification triggers.
  // pinMode(H, INPUT);
  // pinMode(L, INPUT);

  // Calculate PPM to set "whos active" channel to
  for (int i = 0; i < _receiverCount; i++)
  {
    //find first value, then afterwards just increment first value with the distance between intervals
    if (i == 0)
      _ppmWhoValues[i] = (PPM_FOR_EACH_RECEIVER / 2) + 1000;
    else
      _ppmWhoValues[i] = _ppmWhoValues[i - 1] + (PPM_FOR_EACH_RECEIVER);
  }

  // Start serial.
  for (int i = 0; i < _receiverCount; i++)
  {
    receivers[i]->begin(BAUDRATE);
  }

  //set output to FC, so we can transmit to FC
  _output = FC_TX;

  // instantiate timers
  _last_heard = _milli_prev = millis();
}

void MultiRX::updateLoop()
{
  if (_activeReceiver != -1)
  { // If any receiver is active, passtrough, update connection.
    passthrough();
    check_connection();
  }
  else
  { // Otherwise scan for connected receivers and send spoofed data.
    send_spoofed();
    scan_for_connections();
  }
}

/******** Alternate between listening for data from receivers. *********/
void MultiRX::scan_for_connections()
{
  // get current time and get diff from last check
  _milli_curr = millis();
  _milli_diff = _milli_curr - _milli_prev;

  // if timeout, increment what to scan
  if (_milli_diff > RX_LISTEN)
  {

    // go to next receiver
    NextReceiver();

    // turn off all receiver
    turnOffAllReceivers();

    // turn on next receiver
    digitalWrite(powerPins[_scanReceiver], HIGH);

    // save timestamp
    _milli_prev = millis();
  }

  // scan active receiver
  if (receivers[_scanReceiver]->available() > 0 && receivers[_scanReceiver]->read() == PREAMBLE)
  {
    // we got data. start listening to this
    _last_heard = millis();
    _input = receivers[_scanReceiver];
    _activeReceiver = _scanReceiver;
  }
}

/******************** Check that satellite is still active. ********************/
void MultiRX::check_connection()
{
  if (millis() - _last_heard < RX_TIMEOUT)
  { // If active connection, return.
    return;
  }
  else
  { // Else, power off satellites and reset booleans.
    turnOffAllReceivers();
    _activeReceiver = -1; //none active

    NextReceiver();        // as the current scan target is the one that we just lost contact with, it makes sense to scan the next one first
    digitalWrite(powerPins[_scanReceiver], HIGH); // turn on next receiver
    
    _milli_prev = millis(); // Update timestamp to start scan off correctly.

    // set spoof timer to now, to measure time diff
    _datamix_timeout = _milli_prev;
  }
}

/******************* Passthrough of received satellite data. *******************/
void MultiRX::passthrough()
{
  // ensure we wait a secound after changeover to avoid data mixing.
  //  Doing like so to avoid halting entire program with delay
  unsigned long currentMS = millis();
  if ((currentMS - _datamix_timeout) > DATA_DELAY)
  {

    // If data available
    if (_input->available() > 0)
    {
      _last_heard = currentMS; // Update timestamp.

      // get new byte
      uint8_t curr = _input->read();

      // send data to protocol class for framing and parsing
      protocolLib.receiveData(curr, currentMS);

      // if lib say we got a full frame, we got a full frame
      if (protocolLib.gotNewFrame())
      {
        // get the frame
        frame nframe = protocolLib.getNewFrame();

        // if pilot_channel exists in frame we need to modify it depending on what RX is active
        setPilotConnected(&nframe);

        // option to do more modifying if needed...

        // send full frame to FC
        protocolLib.sendFrame(&nframe, _output);
      }
    }
  }
}
/****************** Send spoofed frames to flight controller. ******************/
void MultiRX::send_spoofed()
{
  // We should wait 11ms between each frame we send
  unsigned long now = millis();
  if ((now - _datamix_timeout) > DATA_DELAY)
  {

    //todo if we get IO input that tells us to change spoof message, change it
    //todo figure out if we can just send one msg with only some channels, or we need to cover all channels with 2 half-frames

    // send data
    protocolLib.sendFrame(&_spoofFrame1, _output);
    // wait 5ms between frames
    delay(6);
    protocolLib.sendFrame(&_spoofFrame2, _output);

    // update timestamp for next transmission, to have better control over transmission rate
    _datamix_timeout = now;
  }
}

void MultiRX::setPilotConnected(frame *nframe)
{
  for (int i = 0; i < FRAME_CHANNELS; i++)
  {
    if (nframe->channels[i]._id == CHANNEL_AUX3_PILOT_CONNECT)
    {
      // we have pilot channel in msg, change it to what rx is connected
      nframe->channels[i]._value = _ppmWhoValues[_activeReceiver];
    }
  }
  return;
}

// helper to check if array has any element that is true. Complexity of O(n)
void MultiRX::turnOffAllReceivers()
{
  for (int i = 0; i < _receiverCount; i++)
    digitalWrite(powerPins[i], LOW);
}

// helper function that increments active element in array, and if at end, wraps around
void MultiRX::NextReceiver()
{
  // if we are at last receiver. (receiverCount is not 0 indexed)
  if (_scanReceiver == (_receiverCount - 1))
  {
    //go to first receiver
    _scanReceiver = 0;
    return;
  }
  else
  {
    // go to next receiver
    _scanReceiver++;
  }
  return;
}