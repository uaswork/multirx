#ifndef MULTIRX_CONFIG_FILE_H
#define MULTIRX_CONFIG_FILE_H

#include <HardwareSerial.h>

// Fill in the options of the serial ports to use, 
#define RECEIVER_COUNT  2       //number of connected serial ports. must be equal to the size of arrays below
#define BAUDRATE        115200  //baudrate for the receivers connected on serial

// Add the serial ports and power pins for the number of connected recerivers. Each receiver must have a serial, and a power pin configured for the same index.
//  inline is required as we define and declare them in the header. Otherwise we get "multiple definitions" error
inline unsigned int powerPins[] = {POWR1, POWR2};
inline HardwareSerial* receivers[] = {&Serial1, &Serial2};

// select the serial that has TX to FC
inline HardwareSerial* FC_TX = &Serial1; 

/*
#### to change the spoofed messages which get sent to the FC when no receiver is connected, you have to modify the frames in dualrx.h for now ####
*/

#endif //MULTIRX_CONFIG_FILE_H