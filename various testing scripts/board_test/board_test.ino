/**
 * Blink
 *
 * Turns on an LED on for one second,
 * then off for one second, repeatedly.
 */
#include "Arduino.h"

// Set LED_BUILTIN if it is not defined by Arduino framework
// #define LED_BUILTIN 13

void setup()
{
  // initialize LED digital pin as an output.
  pinMode(LED_BUILTIN, OUTPUT); // PA15
  pinMode(POWR1, OUTPUT); // PA15
  pinMode(POWR2, OUTPUT); // PA15
  // pinMode(PWR1, OUTPUT); // PA15
  // pinMode(PB10, OUTPUT); // PA15
  // pinMode(PB11, OUTPUT); // PA15
  // digitalWrite(PB11, LOW);
  // pinMode(PA08, OUTPUT); // PA15
  digitalWrite(POWR1, HIGH);
  digitalWrite(POWR2, LOW);
}

void loop()
{
  digitalWrite(LED_BUILTIN, HIGH); // sets the digital pin 13 on
  // digitalWrite(PWR1, HIGH); // sets the digital pin 13 on
  // digitalWrite(PB10, HIGH); // sets the digital pin 13 on
  delay(200);            // waits for a second
  digitalWrite(LED_BUILTIN, LOW);  // sets the digital pin 13 off
  // digitalWrite(PWR1, LOW);  // sets the digital pin 13 off
  // digitalWrite(PB10, LOW);  // sets the digital pin 13 off
  delay(200);            // waits for a second
}