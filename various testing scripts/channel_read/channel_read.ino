/********************************************************************************
# Spektrum Satellite Channel Read
# Copyright (c) 2020, Kristian Husum Terkildsen <khte@mmmi.sdu.dk>/
#                                              <kristian.h.terkildsen@gmail.com>
# SDU UAS Center, http://sdu.dk/uas 
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the copyright holder nor the names of its
#      contributors may be used to endorse or promote products derived from
#      this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#********************************************************************************

This softare is used for reading the channel addresses in order to decipher the 
  bytes for modifying data.

Inspired by a sketch made by Kjeld Jensen.

Revision
2020-03-21 KHT First version
****************************************************************************/

#define PIN_RX1_POWER 2
#define PIN_RX2_POWER 3

unsigned long last_rx;
char new_frame;
char high_byte;
char preamble;
unsigned char high, low;

unsigned short addr;

void setup(){
  //Power on RX1
  pinMode(PIN_RX1_POWER, OUTPUT); 
  digitalWrite(PIN_RX1_POWER, HIGH);

  //Start serial
  Serial.begin(115200); // Debug
  Serial1.begin(115200); // RX1 and RX OUT

  last_rx = millis();
  new_frame = false;
  high_byte = false;
}

void loop(){
  if (Serial1.available() > 0){
    Serial.println(Serial1.read());
    /*
    last_rx = millis();
    if (new_frame == true){
      new_frame = false;
      preamble = true;
      high_byte = false;
    }
    if (high_byte == false){
      high = Serial1.read();
      high_byte = true;
    }
    else{
      low = Serial1.read();
      high_byte = false;
      if (preamble == true){
        preamble = false;
      }
      else{
        addr = (high>>3) & 0x0f; //  inspired by dsm.c line 880 to 890
        //Change comparing value below to display various channels and their values.
        if (addr == 4){ // 0 = thr, 1 = rol, 2 = pit, 3 = yaw, 4 = kill switch, 5 = flight mode, 6 = D, 7 = ?, 8 = ?, 9 = ?, 10 = ? DX9
          Serial.println(((high & 0x07)<<8) | low );
          Serial.print("high: ");
          Serial.println(high);
          Serial.print("low: ");
          Serial.println(low);
          Serial.println(" ");
        }
      }
    }
  }
  else if (last_rx + 10 < millis()){
    if (new_frame == false){
      new_frame = true;
    }
    */
  }
}
