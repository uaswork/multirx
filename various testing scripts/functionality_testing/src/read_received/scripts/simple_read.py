#!/usr/bin/env python
#*****************************************************************************
# Simple reader of Spektrum packages from Satellite
# Copyright (c) 2020, Kristian Husum Terkildsen <khte@mmmi.sdu.dk>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    1 Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    2 Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    3 Neither the name of the copyright holder nor the names of its
#      contributors may be used to endorse or promote products derived from
#      this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#*****************************************************************************
# Revisions:
#   2020-05-28 KHT First version:

# Imports.
import rospy
import serial

class simple_read():
    def __init__(self):
        # Connection variables.
        self.usb = '/dev/ttyACM1' #udev rule will be added later
        self.baud = 115200

        # Booleans.
        self.start_detect = False
        self.new_frame = False
        self.high_byte = False

    def try_connection(self):
        try:
            self.ser = serial.Serial(self.usb, self.baud)
            return True
        except Exception as e:
            print 'Serial not found'
            return False

    def find_frame_end(self):
        if not self.start_detect:
            self.prev_byte = self.char
            self.start_detect = True
        elif ord(self.prev_byte) == 101 and ord(self.char) == 0:
            self.new_frame = True
        self.prev_byte = self.char

    def read_channel_val(self):
        self.char = self.ser.read()
        if len(self.char) > 0:
            if self.new_frame:
                if not self.high_byte:
                    self.high = ord(self.char)
                    self.high_byte = True
                else:
                    low = ord(self.char)
                    self.high_byte = False
                    addr = (self.high>>3) & 0x0f
                    val = ((self.high & 0x07)<<8) | low
                    if addr == 0 and val == 178:
                        pass # Ignore preamble.
                    elif addr == 0: # Print channel 0 (throttle)
                        print addr, val, '              '
            else:
                self.find_frame_end()

def main():
    sr = simple_read()
    if sr.try_connection():
        while True:
            sr.read_channel_val()

if __name__ == '__main__':
    main()
