/********************************************************************************
# Dual Spektrum Binding Software
# Copyright (c) 2020, Kristian Husum Terkildsen <khte@mmmi.sdu.dk>/
#                                              <kristian.h.terkildsen@gmail.com>
# SDU UAS Center, http://sdu.dk/uas 
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the copyright holder nor the names of its
#      contributors may be used to endorse or promote products derived from
#      this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#********************************************************************************

This softare is used for binding two SPM4648 Spektrum receiveer through an Aruino 
  Mega 2560. Will be redundant once PCB is made.

Revision
2020-06-09 KHT First version: Basic functionality.
********************************************************************************/

// Pins for powering Spektrum satellites.
#define PIN_RX1_POWER 2
#define PIN_RX2_POWER 3

void setup(){
   pinMode(PIN_RX1_POWER, OUTPUT);
   pinMode(PIN_RX2_POWER, OUTPUT);
   pinMode(LED_BUILTIN, OUTPUT);
}

void loop(){
  digitalWrite(PIN_RX1_POWER, HIGH);
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(20000);
  digitalWrite(PIN_RX1_POWER, LOW);
  digitalWrite(PIN_RX2_POWER, HIGH);
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(20000);
  digitalWrite(PIN_RX2_POWER, LOW);
}
